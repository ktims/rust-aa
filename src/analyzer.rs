use num::{Float, FromPrimitive, Num, ToPrimitive};
use realfft::{RealFftPlanner, RealToComplex};
use ringbuf::{HeapRb, Rb};
use rustfft::num_complex::Complex;
use rustfft::FftNum;

use std::fmt::{Debug, Display};
use std::iter::repeat;
use std::ops::{AddAssign, Mul};
use std::sync::{Arc, Mutex};

pub struct DbMeasurement<U: Float + Sized>(U);
pub struct FrequencyMeasurement<U: Float + Sized>(U);

pub enum FftMeasurementValue<U: Float + Sized> {
    Db(DbMeasurement<U>),
    Peak(FrequencyMeasurement<U>, DbMeasurement<U>),
}

impl<U: Float + FromPrimitive + Display> std::fmt::Display for FftMeasurementValue<U> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Db(v) => v.fmt(f),
            Self::Peak(hz, v) => f.write_fmt(format_args!("{:.0} @ {:.0}", v, hz)),
        }
    }
}

impl<U: Float + FromPrimitive + Display> Display for DbMeasurement<U> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("{:.2}dB", lin_to_db(self.0)))
    }
}

impl<U: Float + Display> Display for FrequencyMeasurement<U> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("{:.0}Hz", self.0))
    }
}

pub trait FftMeasurement<T: Num, U: FftNum + Float, const NUM_CHANNELS: usize>:
    FftMeasurementImpl<T, U> + Send
{
    fn value(&self, channel: usize) -> FftMeasurementValue<U>;
    fn name(&self) -> &'static str;
}

pub trait FftMeasurementImpl<T: Num, U: FftNum + Float> {
    fn accum_td_sample(&mut self, sample: T, channel: usize);
    fn accum_fd_sample(&mut self, bin: (U, U, U), channel: usize); // Slice of tuples of (freq, amplitude)
    fn finalize(&mut self, channel: usize);
}

pub struct RmsAmplitudeMeasurement<U: FftNum, const NUM_CHANNELS: usize> {
    accums: [U; NUM_CHANNELS],
    n_samples: [usize; NUM_CHANNELS],
    last_results: [U; NUM_CHANNELS],
}

impl<U: FftNum, const NUM_CHANNELS: usize> Default for RmsAmplitudeMeasurement<U, NUM_CHANNELS> {
    fn default() -> Self {
        Self {
            accums: [U::zero(); NUM_CHANNELS],
            n_samples: [0; NUM_CHANNELS],
            last_results: [U::zero(); NUM_CHANNELS],
        }
    }
}

impl<T: Num + ToPrimitive, U: FftNum + Float + AddAssign, const NUM_CHANNELS: usize>
    FftMeasurementImpl<T, U> for RmsAmplitudeMeasurement<U, NUM_CHANNELS>
{
    fn accum_fd_sample(&mut self, _: (U, U, U), _: usize) {}
    fn accum_td_sample(&mut self, sample: T, channel: usize) {
        // 'square'
        let f_sample = U::from(sample).unwrap_or(U::zero());
        self.accums[channel] += f_sample * f_sample;
        self.n_samples[channel] += 1;
    }
    fn finalize(&mut self, channel: usize) {
        // 'root mean'
        self.last_results[channel] =
            self.accums[channel] / U::from(self.n_samples[channel]).unwrap_or(U::one());

        self.accums[channel] = U::zero();
        self.n_samples[channel] = 0;
    }
}

impl<T: Num + ToPrimitive, U: FftNum + Float + AddAssign, const NUM_CHANNELS: usize>
    FftMeasurement<T, U, NUM_CHANNELS> for RmsAmplitudeMeasurement<U, NUM_CHANNELS>
{
    fn value(&self, channel: usize) -> FftMeasurementValue<U> {
        FftMeasurementValue::Db(DbMeasurement(self.last_results[channel]))
    }
    fn name(&self) -> &'static str {
        "RMS Amplitude"
    }
}

pub struct PeakFreqAmplitude<U: FftNum, const NUM_CHANNELS: usize> {
    cur_peaks: [(U, U, U); NUM_CHANNELS],
    last_peaks: [(U, U, U); NUM_CHANNELS],
}

impl<U: FftNum, const NUM_CHANNELS: usize> Default for PeakFreqAmplitude<U, NUM_CHANNELS> {
    fn default() -> Self {
        Self {
            cur_peaks: [(U::zero(), U::zero(), U::zero()); NUM_CHANNELS],
            last_peaks: [(U::zero(), U::zero(), U::zero()); NUM_CHANNELS],
        }
    }
}

impl<T: Num + ToPrimitive, U: FftNum + Float + AddAssign, const NUM_CHANNELS: usize>
    FftMeasurement<T, U, NUM_CHANNELS> for PeakFreqAmplitude<U, NUM_CHANNELS>
{
    fn name(&self) -> &'static str {
        "Peak Amplitude"
    }
    fn value(&self, channel: usize) -> FftMeasurementValue<U> {
        FftMeasurementValue::Peak(
            FrequencyMeasurement(self.last_peaks[channel].0),
            DbMeasurement(self.last_peaks[channel].1),
        )
    }
}

impl<T: Num + ToPrimitive, U: FftNum + Float + AddAssign, const NUM_CHANNELS: usize>
    FftMeasurementImpl<T, U> for PeakFreqAmplitude<U, NUM_CHANNELS>
{
    fn accum_fd_sample(&mut self, bin: (U, U, U), channel: usize) {
        if bin.1 > self.cur_peaks[channel].1 {
            self.cur_peaks[channel] = bin
        }
    }
    fn accum_td_sample(&mut self, _sample: T, _channel: usize) {}
    fn finalize(&mut self, channel: usize) {
        self.last_peaks[channel] = self.cur_peaks[channel]
    }
}

pub trait Analyzer<T: Num + Send, U: FftNum + Float + Send, const NUM_CHANNELS: usize> {
    /// Process some data, and return whether the analysis was updated as a result
    fn process_data(&mut self, data: &[T]) -> bool;
    fn set_samplerate(&mut self, rate: U);
    fn set_length(&mut self, length: usize);
    fn set_channels(&mut self, channels: usize);
    fn add_measurement(&mut self, measurement: impl FftMeasurement<T, U, NUM_CHANNELS> + 'static);
    fn log_plot(&self) -> bool;
    fn set_log_plot(&mut self, log_plot: bool);
}

#[derive(Debug, Clone, Default)]
pub struct FftAnalysis<U> {
    pub fft: Vec<(U, U, U)>, // Frequency, Magnitude, Energy
}

struct AudioBuf<T> {
    samples: Vec<HeapRb<T>>, // TODO: Improve data layout
    read_since_last_update: usize,
}

pub trait WindowFunction<T: FftNum> {
    fn amplitude_correction_factor(&self) -> T;
    fn energy_correction_factor(&self) -> T;
    fn apply(&self, buf: &mut [T]);
}

struct HanningWindow<T>(Vec<T>);

impl<T: FftNum> HanningWindow<T> {
    fn new(fft_length: usize) -> Self {
        Self(
            apodize::hanning_iter(fft_length)
                .map(|x| T::from_f64(x).unwrap())
                .collect(),
        )
    }
}

impl<T: FftNum> WindowFunction<T> for HanningWindow<T> {
    fn amplitude_correction_factor(&self) -> T {
        T::from_f64(2.0).unwrap()
    }
    fn energy_correction_factor(&self) -> T {
        T::from_f64(2.0 / num::Float::sqrt(3.0 / 2.0)).unwrap()
    }
    fn apply(&self, buf: &mut [T]) {
        for (a, b) in buf.iter_mut().zip(self.0.iter()) {
            *a = *a * *b
        }
    }
}

pub fn lin_to_db<T: Float + FromPrimitive>(lin: T) -> T {
    T::from_f64(20.0).unwrap_or(T::zero()) * lin.log10()
}

pub fn lin_to_dbfs<T: Float + FromPrimitive>(lin: T) -> T {
    T::from_f64(20.0).unwrap_or(T::zero())
        * (lin * T::from_f64(std::f64::consts::SQRT_2).unwrap_or(T::zero())).log10()
}

// These bounds are really annoying but required to hold RealFftPlanner
pub struct FftAnalyzer<T, U: FftNum, const NUM_CHANNELS: usize> {
    pub last_analysis: Vec<FftAnalysis<U>>,

    fft_length: usize,
    sample_rate: U,
    channels: usize,
    bin_freqs: Vec<U>,

    processor: Arc<dyn RealToComplex<U>>,
    window: HanningWindow<U>,

    audio_buf: Arc<Mutex<AudioBuf<T>>>,

    log_plot: bool,

    pub measurements: Vec<Box<dyn FftMeasurement<T, U, NUM_CHANNELS>>>,
}

impl<T: Num, U: FftNum, const NUM_CHANNELS: usize> FftAnalyzer<T, U, NUM_CHANNELS> {
    pub fn new(fft_length: usize, sample_rate: U, channels: usize) -> Self {
        let mut planner = RealFftPlanner::new();
        let bin_freqs = (0..fft_length)
            .map(|x| {
                U::from_f64(x as f64).unwrap() * sample_rate
                    / U::from_f64(fft_length as f64).unwrap()
            })
            .collect();
        Self {
            last_analysis: Vec::with_capacity(channels),
            fft_length,
            sample_rate,
            channels,
            bin_freqs,

            log_plot: true,

            processor: planner.plan_fft_forward(fft_length),
            window: HanningWindow::new(fft_length),
            audio_buf: Arc::new(Mutex::new(AudioBuf::<T> {
                samples: repeat(0)
                    .take(channels)
                    .map(|_| HeapRb::new(fft_length))
                    .collect(),
                read_since_last_update: 0,
            })),
            measurements: Vec::new(),
        }
    }
    fn time_domain_process<'a>(&mut self, samples: impl Iterator<Item = &'a T>, channel: usize)
    where
        T: Num + Copy + 'static,
        U: Float + Mul + AddAssign + PartialOrd,
    {
        for sample in samples {
            for meas in &mut self.measurements {
                meas.accum_td_sample(*sample, channel);
            }
        }
        for meas in &mut self.measurements {
            meas.finalize(channel);
        }
    }
    fn process_frame(&mut self)
    where
        T: Copy + Clone + Num + ToPrimitive + 'static,
        U: Float + Mul + AddAssign + PartialOrd,
    {
        let _channels_float = U::from_usize(self.channels).unwrap_or(U::one());
        let mut result = Vec::with_capacity(self.channels);

        // Do time domain processing

        for chan in 0..self.channels {
            let raw_in_buf: Vec<_> = self.audio_buf.lock().unwrap().samples[chan]
                .iter()
                .map(|x| *x)
                .collect();
            self.time_domain_process(raw_in_buf.iter(), chan);

            // Transform to float
            let mut in_buf: Vec<U> = raw_in_buf
                .iter()
                .map(|x| U::from(*x).unwrap_or(U::zero()))
                .collect();

            // apply window
            self.window.apply(&mut in_buf);

            let mut out_buf = Vec::from_iter(
                repeat(Complex {
                    re: U::zero(),
                    im: U::zero(),
                })
                .take(self.fft_length / 2 + 1),
            );

            // do fft
            self.processor
                .process(&mut in_buf, out_buf.as_mut_slice())
                .expect("fft failed?");

            let analysis_buf: Vec<_> = out_buf
                .iter()
                .enumerate()
                .map(|(bin, complex)| {
                    // get absolute value and normalize based on length
                    let raw_mag =
                        (complex / U::from_f64(self.fft_length as f64 / 2.0).unwrap()).norm();
                    (
                        self.bin_freqs[bin],
                        raw_mag * self.window.amplitude_correction_factor(),
                        raw_mag * self.window.energy_correction_factor(),
                    )
                })
                .collect();
            for sample in analysis_buf.iter() {
                for meas in &mut self.measurements {
                    meas.accum_fd_sample(*sample, chan);
                }
            }
            result.push(FftAnalysis { fft: analysis_buf });
        }

        self.last_analysis = result;
    }
}

impl<
        T: Copy + Num + Clone + ToPrimitive + Send + 'static,
        U: Float + FftNum + std::ops::AddAssign + Send,
        const NUM_CHANNELS: usize,
    > Analyzer<T, U, NUM_CHANNELS> for FftAnalyzer<T, U, NUM_CHANNELS>
{
    fn process_data(&mut self, data: &[T]) -> bool {
        let _channels_float = U::from(self.channels as f32);

        let mut buf = self.audio_buf.lock().unwrap();
        for chan in 0..self.channels {
            buf.samples[chan]
                .push_iter_overwrite(data.iter().skip(chan).step_by(self.channels).map(|x| *x));
        }
        buf.read_since_last_update += data.len() / self.channels;

        if buf.samples[0].len() >= self.fft_length
            && buf.read_since_last_update >= self.fft_length / 4
        {
            buf.read_since_last_update = 0;
            drop(buf);
            self.process_frame();
            true
        } else {
            false
        }
    }
    fn set_channels(&mut self, channels: usize) {
        self.channels = channels;
    }
    fn set_length(&mut self, _length: usize) {
        unimplemented!(); // Need to rebuild the window and plan
    }
    fn set_samplerate(&mut self, rate: U) {
        self.sample_rate = rate;
    }
    fn add_measurement(&mut self, measurement: impl FftMeasurement<T, U, NUM_CHANNELS> + 'static) {
        self.measurements.push(Box::new(measurement));
    }
    fn set_log_plot(&mut self, log_plot: bool) {
        self.log_plot = log_plot;
    }
    fn log_plot(&self) -> bool {
        self.log_plot
    }
}
