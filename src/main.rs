use cpal::traits::{DeviceTrait, HostTrait, StreamTrait};

use egui::epaint::Hsva;
use egui::Widget;
use egui_plot::{GridInput, GridMark, Legend, Line, Plot};

use std::mem::swap;
use std::sync::atomic::{AtomicUsize, Ordering};
use std::sync::mpsc::{channel, Receiver, Sender};
use std::sync::{Arc, Mutex};
use std::thread::{self, JoinHandle};

mod analyzer;
use analyzer::{
    lin_to_db, Analyzer, FftAnalysis, FftAnalyzer, FftMeasurementValue, PeakFreqAmplitude,
};

use crate::analyzer::RmsAmplitudeMeasurement;

const NUM_CHANNELS: usize = 2;
const SAMPLE_RATES: &[u32] = &[
    11025, 22050, 24000, 32000, 44100, 48000, 88200, 96000, 176400, 192000,
];
const CHANNEL_NAMES: &[&str] = &["Left", "Right"];
const CHANNEL_COLOURS: &[Hsva] = &[
    Hsva {
        h: 0.618,
        s: 0.85,
        v: 0.5,
        a: 0.75,
    },
    Hsva {
        h: 0.0,
        s: 0.85,
        v: 0.5,
        a: 0.75,
    },
];
const DEFAULT_SAMPLE_RATE: u32 = 48000;

const AUDIO_FRAME_SIZE: cpal::BufferSize = cpal::BufferSize::Fixed(512);

type SampleType = f32;
type FloatType = f64;
const FFT_LENGTHS: &[usize] = &[512, 1024, 2048, 4096, 8192, 16384, 32768, 65536, 131072];
const DEFAULT_FFT_LENGTH: usize = 1;

#[derive(Default)]
struct FftPlotData {
    plot_points: Vec<Vec<[f64; NUM_CHANNELS]>>,
    plot_min: f64,
    plot_max: f64,
}

#[derive(Clone, Default)]
struct FftResult<U> {
    fft_analyses: Arc<Mutex<Vec<FftAnalysis<U>>>>,
    plot_data: Arc<Mutex<FftPlotData>>,
}

#[derive(Default)]
struct MyAppUiSelections {
    audio_device: usize,
    sample_rate: usize,
    fft_length: usize,
    x_log: bool,
}

struct MyApp {
    egui_ctx: egui::Context,

    fft_thread: JoinHandle<()>,
    fft_analyzer: Arc<Mutex<FftAnalyzer<SampleType, FloatType, NUM_CHANNELS>>>,
    fft_close_channel: Sender<()>,
    fft_length: usize,
    fft_progress: Arc<AtomicUsize>,

    audio_device: cpal::Device,
    audio_config: cpal::StreamConfig,

    audio_devices: Vec<String>,
    sample_rates: Vec<u32>,
    audio_stream: Option<cpal::Stream>,
    sample_channel: Sender<Vec<SampleType>>, // Store this here so we can restart the audio stream
    last_result: FftResult<FloatType>,

    ui_selections: MyAppUiSelections,
}

struct Db(f64);

trait MeasurementValueType {
    fn label_format(&self) -> String;
}

struct Measurement<T: MeasurementValueType> {
    name: &'static str,
    values: Vec<T>,
}

impl MeasurementValueType for Db {
    fn label_format(&self) -> String {
        format!("{:2.2} dB", self.0)
    }
}

impl<T: MeasurementValueType> Widget for Measurement<T> {
    fn ui(self, ui: &mut egui::Ui) -> egui::Response {
        ui.group(|ui| {
            ui.label(self.name);
            ui.vertical(|ui| {
                for (chan, value) in self.values.iter().enumerate() {
                    ui.colored_label(CHANNEL_COLOURS[chan], value.label_format());
                }
            })
        })
        .response
    }
}

impl eframe::App for MyApp {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        egui::SidePanel::left("left_panel").show(ctx, |ui| {
            ui.label("Capture Options");
            egui::ComboBox::from_label("Source")
                .selected_text(self.audio_devices[self.ui_selections.audio_device].clone())
                .show_ui(ui, |ui| {
                    self.audio_devices
                        .iter()
                        .enumerate()
                        .for_each(|(index, dev)| {
                            ui.selectable_value(&mut self.ui_selections.audio_device, index, dev);
                        });
                });
            self.sample_rate_box(ui);
            self.fft_length_box(ui);

            ui.separator();
            ui.label("Plot Options");
            self.x_log_plot_check(ui);

            ui.separator();
            ui.with_layout(egui::Layout::bottom_up(egui::Align::Center), |ui| {
                self.fft_progress(ui)
            });
        });
        egui::CentralPanel::default().show(ctx, |ui| {
            ui.horizontal(|ui| {
                let lock = self.fft_analyzer.lock().unwrap();
                // Yikes
                let values = lock
                    .measurements
                    .iter()
                    .map(|x| {
                        (
                            x.name(),
                            // FIXME: we need to support more channels someday
                            [x.value(0), x.value(1)],
                        )
                    })
                    .collect::<Vec<_>>();
                drop(lock);
                for meas in values {
                    self.meas_box(ui, meas)
                }
            });
            self.plot_spectrum(ui);
        });
    }
}

fn real_log_grid_spacer(gi: GridInput) -> Vec<GridMark> {
    // we want a major tick at every order of magnitude, integers in the range
    let mut out = Vec::new();
    for i in gi.bounds.0.ceil() as usize..gi.bounds.1.floor() as usize + 1 {
        out.push(GridMark {
            value: i as f64,
            step_size: 1.0,
        });
        // Go from 10^i to 10^(i+1) in steps of (10^i+1 / 10)
        let freq_base = 10.0_f64.powi(i as i32);
        let frac_step = 10.0_f64.powi((i + 1) as i32) / 10.0;
        for frac in 1..10 {
            out.push(GridMark {
                value: (freq_base + frac_step * frac as f64).log10(),
                step_size: 0.1,
            });
        }
    }

    out
}

impl MyApp {
    fn plot_spectrum(&self, ui: &mut egui::Ui) {
        let last_fft_result = self.last_result.clone();
        let plot_data = last_fft_result.plot_data.lock().unwrap();
        let plot_min = plot_data.plot_min.min(-120.0);
        let plot_max = plot_data.plot_max.max(0.0);
        // TODO: Need coordinates_formatter for log and linear plots
        let mut plot = Plot::new("FFT")
            .allow_drag(false)
            .include_y(plot_min)
            .include_y(plot_max)
            .set_margin_fraction(egui::vec2(0.0, 0.0))
            .x_axis_label("Frequency (KHz)")
            .y_axis_label("Magnitude (dBFS)")
            .include_x(0.0)
            .legend(Legend::default());

        plot = if self.ui_selections.x_log {
            plot.include_x((self.sample_rates[self.ui_selections.sample_rate] as f64 / 2.0).log10())
                .x_grid_spacer(real_log_grid_spacer)
                .x_axis_formatter(|x, _n, _r| {
                    if x.floor() == x {
                        format!("{:.0}", 10.0_f64.powf(x))
                    } else {
                        String::default()
                    }
                })
        } else {
            plot.include_x(self.sample_rates[self.ui_selections.sample_rate] as f64 / 2.0)
                .x_axis_formatter(|x, _n, _r| format!("{:.2}", x))
        };

        plot.show(ui, |plot_ui| {
            for (chan, chart) in plot_data.plot_points.iter().enumerate() {
                plot_ui.line(
                    Line::new(chart.to_vec())
                        .fill(plot_min as f32)
                        .name(CHANNEL_NAMES[chan])
                        .color(CHANNEL_COLOURS[chan]),
                );
            }
        });
    }
    fn sample_rate_box(&mut self, ui: &mut egui::Ui) {
        let sample_rate_box = egui::ComboBox::from_label("Sample Rate")
            .selected_text(self.sample_rates[self.ui_selections.sample_rate].to_string());

        sample_rate_box.show_ui(ui, |ui| {
            self.sample_rates
                .iter()
                .enumerate()
                .for_each(|(index, rate)| {
                    ui.selectable_value(
                        &mut self.ui_selections.sample_rate,
                        index,
                        rate.to_string(),
                    );
                });
        });
        if self.sample_rates[self.ui_selections.sample_rate] != self.audio_config.sample_rate.0 {
            self.set_sample_rate(self.sample_rates[self.ui_selections.sample_rate]);
        }
    }
    fn fft_length_box(&mut self, ui: &mut egui::Ui) {
        let fft_length_box = egui::ComboBox::from_label("FFT Length")
            .selected_text(FFT_LENGTHS[self.ui_selections.fft_length].to_string());

        fft_length_box.show_ui(ui, |ui| {
            for (index, length) in FFT_LENGTHS.iter().enumerate() {
                ui.selectable_value(
                    &mut self.ui_selections.fft_length,
                    index,
                    length.to_string(),
                );
            }
        });
        if FFT_LENGTHS[self.ui_selections.fft_length] != self.fft_length {
            self.set_fft_length(FFT_LENGTHS[self.ui_selections.fft_length]);
        }
    }
    fn x_log_plot_check(&mut self, ui: &mut egui::Ui) {
        let mut selection = self.ui_selections.x_log;
        ui.checkbox(&mut selection, "X Log");
        if selection != self.ui_selections.x_log {
            self.ui_selections.x_log = selection;
            self.fft_analyzer.lock().unwrap().set_log_plot(selection);
        }
    }
    fn fft_progress(&mut self, ui: &mut egui::Ui) {
        let percent = self.fft_progress.load(Ordering::Relaxed) as f32 / self.fft_length as f32;
        let fft_progress = egui::ProgressBar::new(percent);
        ui.add(fft_progress);
        if percent >= 1.0 {
            self.fft_progress.store(0, Ordering::Relaxed);
        }
    }
    fn meas_box(
        &mut self,
        ui: &mut egui::Ui,
        meas: (&str, [FftMeasurementValue<FloatType>; NUM_CHANNELS]),
    ) {
        ui.group(|ui| {
            ui.vertical(|ui| {
                ui.label(meas.0);
                for chan in 0..NUM_CHANNELS {
                    ui.colored_label(CHANNEL_COLOURS[chan], meas.1[chan].to_string());
                }
            })
        });
    }
    fn new(cc: &eframe::CreationContext) -> Self {
        let host = cpal::default_host();
        println!("Audio host: {}", host.id().name());

        let (sample_tx, sample_rx) = channel::<Vec<SampleType>>();

        let device_names: Vec<_> = host
            .input_devices()
            .expect("No available input devices")
            .map(|x| x.name().unwrap())
            .collect();
        let default_device = host.default_input_device().unwrap();
        let mut supported_configs = default_device.supported_input_configs().unwrap();
        let supported_sample_rates: Vec<_> = SAMPLE_RATES
            .iter()
            .filter(|x| {
                supported_configs
                    .any(|y| **x <= y.max_sample_rate().0 && **x >= y.min_sample_rate().0)
            })
            .map(|x| *x)
            .collect();

        let sample_rate_idx = supported_sample_rates
            .iter()
            .position(|x| *x == DEFAULT_SAMPLE_RATE)
            .unwrap_or(0);

        let audio_config = cpal::StreamConfig {
            channels: NUM_CHANNELS as u16,
            sample_rate: cpal::SampleRate(supported_sample_rates[sample_rate_idx]),
            buffer_size: AUDIO_FRAME_SIZE,
        };

        let fft_thread_ctx = cc.egui_ctx.clone();
        let last_result = FftResult::<FloatType>::default();
        let fft_thread_result = last_result.clone();

        let fft_close_channel = channel::<()>();

        let mut fft_analyzer = FftAnalyzer::<SampleType, FloatType, NUM_CHANNELS>::new(
            FFT_LENGTHS[DEFAULT_FFT_LENGTH],
            supported_sample_rates[sample_rate_idx] as f64,
            NUM_CHANNELS,
        );
        fft_analyzer.add_measurement(RmsAmplitudeMeasurement::default());

        let fft_analyzer = Arc::new(Mutex::new(fft_analyzer));
        let fft_thread_analyzer_ref = fft_analyzer.clone();

        Self {
            egui_ctx: cc.egui_ctx.clone(),
            fft_thread: thread::Builder::new()
                .name("fft".into())
                .spawn(move || {
                    fft_thread_impl(
                        fft_thread_analyzer_ref,
                        fft_thread_ctx,
                        sample_rx,
                        fft_close_channel.1,
                        fft_thread_result,
                    )
                })
                .unwrap(),
            fft_analyzer,
            fft_length: FFT_LENGTHS[DEFAULT_FFT_LENGTH],
            fft_progress: Arc::new(AtomicUsize::new(0)),
            fft_close_channel: fft_close_channel.0,
            ui_selections: MyAppUiSelections {
                audio_device: device_names
                    .iter()
                    .position(|x| *x == default_device.name().unwrap())
                    .unwrap_or(0),
                sample_rate: sample_rate_idx,
                fft_length: DEFAULT_FFT_LENGTH,
                x_log: true,
            },
            sample_rates: supported_sample_rates,
            audio_device: default_device,
            audio_stream: None,
            audio_devices: device_names,
            last_result,
            sample_channel: sample_tx,
            audio_config,
        }
    }
    fn run(&mut self) {
        self.audio_thread();
    }
    fn restart_threads(&mut self) {
        let sample_channel = channel::<Vec<SampleType>>();
        let close_channel = channel::<()>();

        // Stop the audio first so it doesn't try to send to a closed channel
        self.audio_stream = None;

        // Stop fft thread, rebuild it, swap with the old and join on the old thread
        self.fft_close_channel.send(()).unwrap();
        let mut new_thread = self.fft_thread(sample_channel.1, close_channel.1);
        swap(&mut self.fft_thread, &mut new_thread);
        new_thread.join().unwrap();

        // Replace the channels in self
        self.fft_close_channel = close_channel.0;
        self.sample_channel = sample_channel.0;

        // Restart the audio stream
        self.audio_thread();
    }
    fn set_sample_rate(&mut self, rate: u32) {
        match self.sample_rates.iter().position(|x| *x == rate) {
            Some(rate_idx) => {
                self.ui_selections.sample_rate = rate_idx;
                self.audio_config.sample_rate = cpal::SampleRate(SAMPLE_RATES[rate_idx]);
                self.restart_threads();
            }
            None => unimplemented!("Make some graceful error handler"),
        }
    }
    fn set_fft_length(&mut self, length: usize) {
        match FFT_LENGTHS.iter().position(|x| *x == length) {
            Some(length_idx) => {
                self.fft_length = length;
                self.ui_selections.fft_length = length_idx;
                self.restart_threads();
            }
            None => unimplemented!("Make some graceful error handler"),
        }
    }
    fn audio_thread(&mut self) {
        if self.audio_stream.is_some() {
            self.audio_stream = None;
        }
        println!(
            "Starting audio thread on device `{}` with config `{:?}`",
            self.audio_device.name().unwrap(),
            self.audio_config
        );

        let new_channel = self.sample_channel.clone();
        let fft_progress_ref = self.fft_progress.clone();

        self.audio_stream = Some(
            self.audio_device
                .build_input_stream(
                    &self.audio_config,
                    move |data, _| {
                        handle_audio_frame(data, new_channel.clone(), fft_progress_ref.clone())
                    },
                    move |err| panic!("{:?}", err),
                    Some(std::time::Duration::from_secs(1)),
                )
                .unwrap(),
        );
        println!("Stream built");
        self.audio_stream.as_ref().unwrap().play().unwrap(); // this doesn't block
    }

    fn fft_thread(
        &mut self,
        data_pipe: Receiver<Vec<SampleType>>,
        close_pipe: Receiver<()>,
    ) -> JoinHandle<()> {
        let result_ref = self.last_result.clone();
        let ctx_ref = self.egui_ctx.clone();
        let mut fft_analyzer = FftAnalyzer::<SampleType, FloatType, NUM_CHANNELS>::new(
            FFT_LENGTHS[self.ui_selections.fft_length],
            self.sample_rates[self.ui_selections.sample_rate] as f64,
            NUM_CHANNELS,
        );
        fft_analyzer.add_measurement(RmsAmplitudeMeasurement::default());
        fft_analyzer.add_measurement(PeakFreqAmplitude::default());

        *self.fft_analyzer.lock().unwrap() = fft_analyzer;

        let fft_thread_analyzer_ref = self.fft_analyzer.clone();

        thread::Builder::new()
            .name("fft".into())
            .spawn(move || {
                fft_thread_impl(
                    fft_thread_analyzer_ref,
                    ctx_ref,
                    data_pipe,
                    close_pipe,
                    result_ref,
                )
            })
            .unwrap()
    }
}

fn plot_min(data: &Vec<Vec<[f64; 2]>>) -> f64 {
    let mut min = f64::INFINITY;
    for channel in data {
        for point in channel {
            if point[1] < min && point[1] > f64::NEG_INFINITY {
                min = point[1];
            }
        }
    }
    min.clamp(-160.0, 160.0)
}

fn plot_max(data: &Vec<Vec<[f64; 2]>>) -> f64 {
    let mut max = f64::NEG_INFINITY;
    for channel in data {
        for point in channel {
            if point[1] > max && point[1] < f64::INFINITY {
                max = point[1];
            }
        }
    }
    max.clamp(-160.0, 160.0)
}

fn fft_thread_impl(
    analyzer: Arc<Mutex<FftAnalyzer<SampleType, FloatType, NUM_CHANNELS>>>,
    ctx: egui::Context,
    data_pipe: Receiver<Vec<SampleType>>,
    close_pipe: Receiver<()>,
    last_result: FftResult<FloatType>,
) {
    println!("FFT thread initialized");
    loop {
        if close_pipe.try_recv().is_ok() {
            println!("FFT thread ended");
            return;
        }
        if let Ok(buf) = data_pipe.recv_timeout(std::time::Duration::from_secs_f64(0.1)) {
            let new_result = analyzer.lock().unwrap().process_data(&buf);
            if new_result {
                // Prepare the data for plotting
                let lock = analyzer.lock().unwrap();
                let charts: Vec<Vec<[f64; 2]>> =
                    if lock.log_plot() {
                        lock.last_analysis
                            .iter()
                            .map(|analysis| {
                                Vec::from_iter(analysis.fft.iter().map(|(freq, amp, _energy)| {
                                    [
                                        (*freq as f64).log10().clamp(0.0, f64::INFINITY),
                                        lin_to_db(*amp as f64),
                                    ]
                                }))
                            })
                            .collect()
                    } else {
                        lock.last_analysis
                            .iter()
                            .map(|analysis| {
                                Vec::from_iter(analysis.fft.iter().map(|(freq, amp, _energy)| {
                                    [*freq as f64, lin_to_db(*amp as f64)]
                                }))
                            })
                            .collect()
                    };
                drop(lock);

                let plot_min = plot_min(&charts);
                let plot_max = plot_max(&charts);

                let mut plot_data = last_result.plot_data.lock().unwrap();
                *plot_data = FftPlotData {
                    plot_points: charts,
                    plot_min,
                    plot_max,
                };
                // while holding the plot_lock which will block the gui thread, update the rest
                *last_result.fft_analyses.lock().unwrap() =
                    analyzer.lock().unwrap().last_analysis.clone();
                drop(plot_data);
                ctx.request_repaint();
            }
        }
    }
}

fn handle_audio_frame(
    data: &[SampleType],
    chan: Sender<Vec<SampleType>>,
    fft_progress: Arc<AtomicUsize>,
) {
    // Copy and send to processor thread
    chan.send(data.to_vec()).unwrap_or_default();
    fft_progress.store(
        fft_progress.load(Ordering::Relaxed) + data.len(),
        Ordering::Relaxed,
    );
}

fn main() {
    env_logger::init();
    let options = eframe::NativeOptions {
        initial_window_size: Some(egui::vec2(1024.0, 768.0)),
        ..Default::default()
    };

    println!("App initialized");

    eframe::run_native(
        "rust-aa",
        options,
        Box::new(|cc| {
            let mut app = Box::new(MyApp::new(cc));
            app.run();
            app
        }),
    )
    .unwrap();
}
